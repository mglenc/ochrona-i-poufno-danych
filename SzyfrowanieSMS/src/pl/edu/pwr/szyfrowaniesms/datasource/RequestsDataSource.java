package pl.edu.pwr.szyfrowaniesms.datasource;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Request;

public class RequestsDataSource {
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	private String[] allColumns = { Request.COLUMN_ID, Request.COLUMN_CONTACT, Request.COLUMN_CONTENT, Request.COLUMN_STATUS, Request.COLUMN_CREATED_AT, Request.COLUMN_MODIFICATION_AT};
	
	public RequestsDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public Request createRequest(Contact contact, String content, Integer status) {
		ContentValues values = new ContentValues();
		values.put(Request.COLUMN_CONTACT, contact.getId());
		values.put(Request.COLUMN_CONTENT, content);
		values.put(Request.COLUMN_STATUS, status);
		
		long insertId = database.insert(Request.TABLE_REQUESTS, null, values);
		Cursor cursor = database.query(Request.TABLE_REQUESTS, allColumns, Request.COLUMN_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();

		Request newRequest = cursorToRequest(cursor);
		cursor.close();
		
		return newRequest;
	}
	
	public void deleteRequest(Request request) {
		long id = request.getId();
		Log.w(DatabaseHelper.class.getName(), "Request deleted with id: " + id);
	    database.delete(Request.TABLE_REQUESTS, Contact.COLUMN_ID + " = " + id, null);
	}
	
	public Request getRequestByContact(Contact contact) {
		Cursor cursor = database.query(Request.TABLE_REQUESTS, allColumns, Request.COLUMN_CONTACT + " = " + contact.getId(), null, null, null, null);
		Request ret = null;
		if(cursor.moveToFirst()){
			ret = cursorToRequest(cursor);
		}
		cursor.close();
		return ret;
	}
	
	public void updateRequest(Request request) {
		ContentValues values = new ContentValues();
		values.put(Request.COLUMN_CONTACT, request.getContactId());
		values.put(Request.COLUMN_CONTENT, request.getContent());
		values.put(Request.COLUMN_STATUS, request.getStatus());
		
		database.update(Request.TABLE_REQUESTS, values, Request.COLUMN_ID + " = " + request.getId(), null);
	}
	
	public List<Request> getAllRequests() {
		List<Request> requests = new ArrayList<Request>();
		
		Cursor cursor = database.query(Request.TABLE_REQUESTS, allColumns, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Request request = cursorToRequest(cursor);
				requests.add(request);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return requests;
	}
	
	private Request cursorToRequest(Cursor cursor) {
		Request request = new Request();
		request.setId(cursor.getLong(0));
		
		//foreign key
		request.setContactId(cursor.getLong(1));
		
		request.setContent(cursor.getString(2));
		request.setStatus(cursor.getLong(3));
		
		//date strings
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date created_at = df.parse(cursor.getString(4));
			request.setCreatedAt(created_at);
			Date modification_at = df.parse(cursor.getString(5));
			request.setModificationAt(modification_at);
		} catch (ParseException e) {
			Log.e(RequestsDataSource.class.getName(), "Failed to parse datetime.", e);
		}

		return request;
	}
}
