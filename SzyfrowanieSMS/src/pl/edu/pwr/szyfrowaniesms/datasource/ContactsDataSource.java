package pl.edu.pwr.szyfrowaniesms.datasource;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ContactsDataSource {
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	private String[] allColumns = { Contact.COLUMN_ID, Contact.COLUMN_FULLNAME, Contact.COLUMN_NUMBER, Contact.COLUMN_PUBKEY };
	
	public ContactsDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public Contact createContact(String fullname, String number, String pub_key) {
		ContentValues values = new ContentValues();
		values.put(Contact.COLUMN_FULLNAME, fullname);
		values.put(Contact.COLUMN_NUMBER, number);
		if(pub_key != null) {
			values.put(Contact.COLUMN_PUBKEY, pub_key);
		} else {
			values.put(Contact.COLUMN_PUBKEY, "");
		}
		
		//String contactData = fullname + ": " + number + " " + pub_key;
		//Log.d("contact data", contactData);
		
		long insertId = database.insert(Contact.TABLE_CONTACTS, null, values);
		Cursor cursor = database.query(Contact.TABLE_CONTACTS, allColumns, Contact.COLUMN_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		Contact newContact = cursorToContact(cursor);
		cursor.close();
		return newContact;
	}
	
	public boolean updateContact(Contact contact){
		ContentValues values = new ContentValues();
		values.put(Contact.COLUMN_FULLNAME, contact.getFullname());
		values.put(Contact.COLUMN_NUMBER, contact.getNumber());
		values.put(Contact.COLUMN_PUBKEY, contact.getPubKey());
		
		return 1 == database.update(Contact.TABLE_CONTACTS, values, Contact.COLUMN_ID + " = " + contact.getId(), null);
	}
	
	public void deleteContact(Contact contact) {
		long id = contact.getId();
		Log.w(DatabaseHelper.class.getName(), "Contact deleted with id: " + id);
	    database.delete(Contact.TABLE_CONTACTS, Contact.COLUMN_ID + " = " + id, null);
	}
	
	public Contact getContactById(long id) {
		Cursor cursor = database.query(Contact.TABLE_CONTACTS, allColumns, Contact.COLUMN_ID  + " = " + id, null, null, null, null, "1");
		Contact ret = null;
		if(cursor.moveToFirst()){
			ret = cursorToContact(cursor);
		}
		cursor.close();
		return ret;
	}
	
	public Contact getContactByNumber(String number){
		Cursor cursor = database.query(Contact.TABLE_CONTACTS, allColumns, Contact.COLUMN_NUMBER  + " = ?", new String[]{number}, null, null, null, "1");
		Contact ret = null;
		if(cursor.moveToFirst()){
			ret = cursorToContact(cursor);
		}
		cursor.close();
		return ret;
	}
	
	public List<Contact> getAllContacts() {
		List<Contact> contacts = new ArrayList<Contact>();
		Cursor cursor = database.query(Contact.TABLE_CONTACTS, allColumns, null, null, null, null, null);
		if(cursor.moveToFirst()){
			while (!cursor.isAfterLast()) {
				Contact contact = cursorToContact(cursor);
				contacts.add(contact);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return contacts;
	}
	
	private Contact cursorToContact(Cursor cursor) {
		Contact contact = new Contact();
		contact.setId(cursor.getLong(0));
		contact.setFullname(cursor.getString(1));
		contact.setNumber(cursor.getString(2));
		contact.setPubKey(cursor.getString(3));
		return contact;
	}
}
