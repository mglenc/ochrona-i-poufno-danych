package pl.edu.pwr.szyfrowaniesms.datasource;

import pl.edu.pwr.szyfrowaniesms.bean.UserData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class UserDataDataSource {
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	private String[] allColumns = { UserData.COLUMN_ID, UserData.COLUMN_NUMBER, UserData.COLUMN_PASSWORD, UserData.COLUMN_PUBLICKEY, UserData.COLUMN_PRIVATEKEY };
	
	public UserDataDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public UserData createUserData(String number, String password, String publicKey, String privateKey) {
		ContentValues values = new ContentValues();
		values.put(UserData.COLUMN_NUMBER, number);
		values.put(UserData.COLUMN_PASSWORD, password);
		values.put(UserData.COLUMN_PUBLICKEY, publicKey);
		values.put(UserData.COLUMN_PRIVATEKEY, privateKey);
		
		long insertId = database.insert(UserData.TABLE_USERDATA, null, values);
		Cursor cursor = database.query(UserData.TABLE_USERDATA, allColumns, UserData.COLUMN_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		UserData newUserData = cursorToUserData(cursor);
		cursor.close();
		return newUserData;
	}
	
	public UserData getUserDataByNumber(String number){
		Cursor cursor = database.query(UserData.TABLE_USERDATA, allColumns, UserData.COLUMN_NUMBER  + " = " + number, null, null, null, null, "1");
		UserData ret = null;
		if(cursor.moveToFirst()){
			ret =  cursorToUserData(cursor);
		}
		cursor.close();
		return ret;
	}
	
	public UserData getUserDataById(Long id){
		Cursor cursor = database.query(UserData.TABLE_USERDATA, allColumns, UserData.COLUMN_ID  + " = " + id, null, null, null, null, "1");
		UserData ret = null;
		if(cursor.moveToFirst()){
			ret =  cursorToUserData(cursor);
		}
		cursor.close();
		return ret;
	}
	
	private UserData cursorToUserData(Cursor cursor) {
		UserData userData = new UserData();
		userData.setId(cursor.getLong(0));
		userData.setNumber(cursor.getString(1));
		userData.setPassword(cursor.getString(2));
		userData.setPublicKey(cursor.getString(3));
		userData.setPrivateKey(cursor.getString(4));
		return userData;
	}
}
