package pl.edu.pwr.szyfrowaniesms.datasource;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Message;

public class MessagesDataSource {
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	private String[] allColumns = { Message.COLUMN_ID, Message.COLUMN_CONTACT, Message.COLUMN_CONTENT, Message.COLUMN_DATETIME, Message.COLUMN_TO_USER};
	
	public MessagesDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public Message createMessage(Contact contact, String content, Date datetime, Integer to_user) {
		ContentValues values = new ContentValues();
		values.put(Message.COLUMN_CONTACT, contact.getId());
		values.put(Message.COLUMN_CONTENT, content);
		values.put(Message.COLUMN_TO_USER, to_user);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dt = dateFormat.format(datetime);
		
		values.put(Message.COLUMN_DATETIME, dt);
		
		long insertId = database.insert(Message.TABLE_MESSAGES, null, values);
		Cursor cursor = database.query(Message.TABLE_MESSAGES, allColumns, Message.COLUMN_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		Message newMessage = cursorToMessage(cursor);
		cursor.close();
		
		return newMessage;
	}
	
	public void updateMessage(Message message) {
		ContentValues values = new ContentValues();
		values.put(Message.COLUMN_CONTACT, message.getContactId());
		values.put(Message.COLUMN_CONTENT, message.getContent());
		values.put(Message.COLUMN_TO_USER, message.getToUser());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dt = dateFormat.format(message.getDatetime());
		
		values.put(Message.COLUMN_DATETIME, dt);
		
		database.update(Message.TABLE_MESSAGES, values,	Message.COLUMN_ID + " = " + message.getId(), null);
	}
	
	public void deleteMessage(Message message) {
		long id = message.getId();
		Log.w(DatabaseHelper.class.getName(), "Message deleted with id: " + id);
	    database.delete(Message.TABLE_MESSAGES, Message.COLUMN_ID + " = " + id, null);
	}
	
	public List<Message> getAllMessages() {
		List<Message> messages = new ArrayList<Message>();
		Cursor cursor = database.query(Message.TABLE_MESSAGES, allColumns, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Message message = cursorToMessage(cursor);
				messages.add(message);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return messages;
	}
	
	public List<Message> getMessagesForContact(Contact contact) {
		List<Message> messages = new ArrayList<Message>();
		Cursor cursor = database.query(Message.TABLE_MESSAGES, allColumns, Message.COLUMN_CONTACT + " = " + contact.getId(), null, null, null, null);
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				Message message = cursorToMessage(cursor);
				messages.add(message);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return messages;
	}
	
	private Message cursorToMessage(Cursor cursor) {
		Message message = new Message();
		message.setId(cursor.getLong(0));
		
		//foreign key
		message.setContactId(cursor.getLong(1));
		
		message.setContent(cursor.getString(2));
		
		//date strings
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date datetime = df.parse(cursor.getString(3));
			message.setDatetime(datetime);
		} catch (ParseException e) {
			Log.e(MessagesDataSource.class.getName(), "Failed to parse datetime.", e);
		}
		
		message.setToUser(cursor.getLong(4));

		return message;
	}
}
