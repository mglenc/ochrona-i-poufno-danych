package pl.edu.pwr.szyfrowaniesms.datasource;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Message;
import pl.edu.pwr.szyfrowaniesms.bean.Request;
import pl.edu.pwr.szyfrowaniesms.bean.UserData;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	static final String DATABASE_NAME = "smscipher.db";
	static final int DATABASE_VERSION = 1;
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		Contact.onCreate(database);
		Request.onCreate(database);
		Message.onCreate(database);
		UserData.onCreate(database);
	}
	
	//Cleaning database and creating new tables
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	  Log.w(DatabaseHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
	  Contact.onUpgrade(db);
	  Request.onUpgrade(db);
	  Message.onUpgrade(db);
	  UserData.onUpgrade(db);
	}
}
