package pl.edu.pwr.szyfrowaniesms;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Request;
import pl.edu.pwr.szyfrowaniesms.cipher.PGPSMSCipher;
import pl.edu.pwr.szyfrowaniesms.datasource.ContactsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.RequestsDataSource;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AnswerRequestActivity extends Activity {
	private ContactsDataSource contacts_datasource;
	private RequestsDataSource requests_datasource;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.answer_request);
		
		//loading contacts datasource
        contacts_datasource = new ContactsDataSource(this);
        
        //loading requests datasource
        requests_datasource = new RequestsDataSource(this);
	}
	
	 @Override
	 protected void onPause() {
		 contacts_datasource.close();
		 requests_datasource.close();
		 super.onPause();
	 }
	 
	 @Override
	 protected void onResume() {
    	super.onResume();
        
    	contacts_datasource.open();
    	requests_datasource.open();
    	
    	final Contact contact = (Contact) getIntent().getSerializableExtra("contact");
    	
    	final TextView requestNumberTextView = (TextView) findViewById(R.id.request_number);
    	requestNumberTextView.setText(contact.getNumber());
    	
    	final Button requestAcceptButton = (Button) findViewById(R.id.request_accept);
    	final Button requestRejectButton = (Button) findViewById(R.id.request_reject);
    	
    	final Context context = this;
    	
    	requestAcceptButton.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				//set request status
  				Request request = requests_datasource.getRequestByContact(contact);
  				request.setStatus(4);
  				requests_datasource.updateRequest(request);
  				  				  				
  				//set contact name
  				AlertDialog.Builder alert = new AlertDialog.Builder(context);
  				alert.setTitle("Set contact's name");
  				alert.setCancelable(false);
  				LinearLayout lila1 = new LinearLayout(context);
  				lila1.setOrientation(LinearLayout.VERTICAL);
  				final EditText inputFullname = new EditText(context);
  				inputFullname.setHint(contact.getFullname());

  				lila1.addView(inputFullname);
  				alert.setView(lila1);
  				alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
  					public void onClick(DialogInterface dialog, int whichButton) {
  						String fullName = inputFullname.getText().toString();
  						if(null == fullName || fullName.isEmpty()){
  							fullName = inputFullname.getHint().toString();
  						}
  						contact.setFullname(fullName);
  						contacts_datasource.updateContact(contact);
  						
  						//send user public key
  		  				PGPSMSCipher pgpsmscipher = new PGPSMSCipher(MainActivity.currentUserData);
  		  				pgpsmscipher.sendPublicKeyMsg(contact.getNumber());
  		  				
  		  				//returning to contact list view
  		  				Intent intent = new Intent(AnswerRequestActivity.this, MainActivity.class);
  		  				startActivity(intent);
  					}
  				});
  				alert.show();
  			}
  		});
    	
    	requestRejectButton.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				//set request status
  				Request request = requests_datasource.getRequestByContact(contact);
  				request.setStatus(5);
  				
  				//TO DO: send reject message
  				
  				//remove contact
  				contacts_datasource.deleteContact(contact);
  				
  				//returning to contact list view
  				Intent intent = new Intent(AnswerRequestActivity.this, MainActivity.class);
  				startActivity(intent);
  			}
  		});
	 }
}
