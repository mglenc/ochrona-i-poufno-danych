package pl.edu.pwr.szyfrowaniesms;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Request;
import pl.edu.pwr.szyfrowaniesms.datasource.ContactsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.RequestsDataSource;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InviteActivity extends Activity {
	private ContactsDataSource contacts_datasource;
	private RequestsDataSource requests_datasource;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//loading contacts datasource
        contacts_datasource = new ContactsDataSource(this);
        
        //loading requests datasource
        requests_datasource = new RequestsDataSource(this);
        
        setContentView(R.layout.invite_main);
  	  
		final Button inviteButton = (Button) findViewById(R.id.invite);
		
		final Context context = this;
          
        inviteButton.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				String fullname = ((EditText)findViewById(R.id.invite_name)).getText().toString();
  				String phoneNumber = ((EditText)findViewById(R.id.invite_phone)).getText().toString();
  				if(fullname.isEmpty() || phoneNumber.isEmpty()){
  					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
  					AlertDialog dialog = alertDialogBuilder.create();
  					dialog.setMessage("Please fill contact's name and phone number.");
  					dialog.show();
  					return;
  				}
  				
  				//creating contact to send request to with null public key
  				Contact contact = contacts_datasource.createContact(fullname, phoneNumber, null);
  				
  				//creating public key request
  				Request request = requests_datasource.createRequest(contact, null, 0);
  				
  				//sending invitation to number, including public key
  				request.sendInvitation(contact, context);
  				
  				Intent intent = new Intent(InviteActivity.this, MainActivity.class);
  				startActivity(intent);
  			}
  		});
	}
	
	 @Override
	 protected void onPause() {
		 contacts_datasource.close();
		 requests_datasource.close();
		 super.onPause();
	 }
	 
	 @Override
	 protected void onResume() {
    	super.onResume();
        
    	contacts_datasource.open();
    	requests_datasource.open();
	 }
}
