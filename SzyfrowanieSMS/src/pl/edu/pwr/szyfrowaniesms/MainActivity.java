package pl.edu.pwr.szyfrowaniesms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Request;
import pl.edu.pwr.szyfrowaniesms.bean.UserData;
import pl.edu.pwr.szyfrowaniesms.cipher.PGPSMSCipher;
import pl.edu.pwr.szyfrowaniesms.cipher.SMSCipher;
import pl.edu.pwr.szyfrowaniesms.datasource.ContactsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.MessagesDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.RequestsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.UserDataDataSource;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

public class MainActivity extends Activity {
	public static UserData currentUserData;
	private ContactsDataSource contacts_datasource;
	private MessagesDataSource messages_datasource;
	private RequestsDataSource requests_datasource;
	private UserDataDataSource userDataDataSource;
	private SMSCipher smsCipher;
	private String phoneNumber;
	
	public static ContactListArrayAdapter contactlistAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactlist_main);
        
        //clearing database
        //this.deleteDatabase(DatabaseHelper.DATABASE_NAME);
        
        //loading data to contact list
        contacts_datasource = new ContactsDataSource(this);
        //loading messages datasource
        messages_datasource = new MessagesDataSource(this);
        userDataDataSource = new UserDataDataSource(this);
        requests_datasource = new RequestsDataSource(this);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
        
    	contacts_datasource.open();
    	messages_datasource.open();
    	userDataDataSource.open();
    	requests_datasource.open();
    	
        if(null == MainActivity.currentUserData){
        	UserData currUserData = userDataDataSource.getUserDataById(1L);
        	showLoginDialog(currUserData);
        }
    	
    	final ListView contactListView = (ListView) findViewById(R.id.contactListView);
        
        List<Contact> contacts = contacts_datasource.getAllContacts();
        
        contactlistAdapter = new ContactListArrayAdapter(this, android.R.layout.simple_list_item_1, contacts);
        contactListView.setAdapter(contactlistAdapter);
                
        final Button newInviteButton = (Button) findViewById(R.id.newInviteButton);
        
        //display invitation view
        newInviteButton.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				Intent intent = new Intent(MainActivity.this, InviteActivity.class);
  				startActivity(intent);
  			}
        });
        
        //display view with contact messages list
        contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
	        	final Contact contact = (Contact) parent.getItemAtPosition(position);
	        	
	        	//checking request status
	        	Request contact_request = requests_datasource.getRequestByContact(contact);
	        	long request_status = contact_request.getStatus();
	        	
	        	//creating switch intent
	        	Intent intent;
	        		        	
	        	switch ((int)request_status) {
	        	case 0:
	        		//waiting for public key
	        		intent = new Intent(MainActivity.this, RequestWaitActivity.class);
		        	intent.putExtra("contact", contact);
		        	startActivity(intent);
	        		break;
	        	case 3:
	        		//send public key or reject
	        		intent = new Intent(MainActivity.this, AnswerRequestActivity.class);
		        	intent.putExtra("contact", contact);
		        	startActivity(intent);
	        		break;
	        	case 1:
	        	case 4:
	        		//accepted, going to message list
	        		intent = new Intent(MainActivity.this, ContactMessagesActivity.class);
		        	intent.putExtra("contact", contact);
		        	startActivity(intent);
	        		break;
	        	case 2:
	        	case 5:
	        		//rejected, deny sending messages
	        		break;
	        	}        	
	        }
        });
    }
    
    class ContactListArrayAdapter extends ArrayAdapter<Contact> {
    	private List<Contact> contacts = new ArrayList<Contact>();

        public ContactListArrayAdapter(Context context, int textViewResourceId, List<Contact> objects) {
        	super(context, textViewResourceId, objects);
	        contacts = objects;
        }
        
        public ContactListArrayAdapter(Context context, int textViewResourceId) {
    		super(context, textViewResourceId);
    	}
        
    	public int getCount() {
    		return this.contacts.size();
    	}

    	public Contact getItem(int index) {
    		return this.contacts.get(index);
    	}

        @Override
        public boolean hasStableIds() {
          return true;
        }
    }

    @Override
    protected void onPause() {
    	contacts_datasource.close();
    	messages_datasource.close();
    	requests_datasource.close();
    	userDataDataSource.close();
    	super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (item.getItemId() == R.id.action_keys) {
        	Intent intent = new Intent(MainActivity.this, KeysActivity.class);
        	startActivity(intent);	 
        	
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showLoginDialog(final UserData currUserData){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Login");
		alert.setCancelable(false);
		LinearLayout lila1 = new LinearLayout(this);
		lila1.setOrientation(LinearLayout.VERTICAL);
		final EditText inputNumber = new EditText(this);
		inputNumber.setInputType(InputType.TYPE_CLASS_PHONE);
		if(null == currUserData){
			inputNumber.setHint("Phone number");
			lila1.addView(inputNumber);
		}else{
			inputNumber.setText(currUserData.getNumber());
		}
		final EditText inputPassword = new EditText(this);
		inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		inputPassword.setHint("Password");
		lila1.addView(inputPassword);
		alert.setView(lila1);
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				MainActivity.this.phoneNumber = inputNumber.getText().toString();
				if (!login(currUserData, inputPassword.getText().toString())) {
					showLoginDialog(currUserData);
				}
			}
		});
		alert.show();
    }
    
    private boolean login(UserData currUserData, String password){
        if(null == currUserData){
        	UserData newUserData = new UserData();
        	newUserData.setNumber(phoneNumber);
        	newUserData.setPassword(new String(Hex.encodeHex(DigestUtils.md5(password.getBytes()))));
        	
        	smsCipher = new PGPSMSCipher(newUserData);
        	if(smsCipher.generateKeyPair(password)){
				if(null != userDataDataSource.createUserData(newUserData.getNumber(),
						newUserData.getPassword(), newUserData.getPublicKey(),
						newUserData.getPrivateKey())){
					newUserData.setPlainPassword(password);
					MainActivity.currentUserData = newUserData;
					return true;
				}
				return false;
        	}
        	return false;
        } else {
        	if(new String(Hex.encodeHex(DigestUtils.md5(password.getBytes()))).equals(currUserData.getPassword())){
        		currUserData.setPlainPassword(password);
        		MainActivity.currentUserData = currUserData;
        		smsCipher = new PGPSMSCipher(currUserData);
        		return true;
        	}
        	return false;
        }
    }
}
