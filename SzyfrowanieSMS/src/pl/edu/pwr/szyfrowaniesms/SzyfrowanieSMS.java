package pl.edu.pwr.szyfrowaniesms;

import android.app.Application;
import android.content.Context;

public class SzyfrowanieSMS extends Application {
	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		SzyfrowanieSMS.context = getApplicationContext();
	}

	public static Context getAppContext() {
		return SzyfrowanieSMS.context;
	}
}
