package pl.edu.pwr.szyfrowaniesms.bean;

import java.util.Date;

import pl.edu.pwr.szyfrowaniesms.MainActivity;
import pl.edu.pwr.szyfrowaniesms.cipher.PGPSMSCipher;
import android.app.AlertDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Request {
	private long id;
	private long contact_id;
	private String content;
	private long status;
	private Date created_at;
	private Date modification_at;
	
	public static final String TABLE_REQUESTS = "requests";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_CONTACT = "contact";
	public static final String COLUMN_CONTENT = "content";
	public static final String COLUMN_STATUS = "status";
	public static final String COLUMN_CREATED_AT = "created_at";
	public static final String COLUMN_MODIFICATION_AT = "modification_at";
	
	private static final String REQUESTS_CREATE = "create table " + TABLE_REQUESTS + "("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_CONTACT + " integer not null, "
			+ COLUMN_CONTENT + " text, "
			+ COLUMN_STATUS + " integer not null,"
			+ COLUMN_CREATED_AT + " datetime default CURRENT_TIMESTAMP,"
			+ COLUMN_MODIFICATION_AT + " datetime default CURRENT_TIMESTAMP,"
			+ " FOREIGN KEY (" + COLUMN_CONTACT + ") REFERENCES " + Contact.TABLE_CONTACTS + " (" + Contact.COLUMN_ID + "));";
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getContactId() {
		return contact_id;
	}
	
	public void setContactId(long contact_id) {
		this.contact_id = contact_id;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public long getStatus() {
		return status;
	}
	
	public void setStatus(long status) {
		this.status = status;
	}
	
	public Date getCreatedAt() {
		return created_at;
	}
	
	public void setCreatedAt(Date created_at) {
		this.created_at = created_at;
	}
	
	public Date getModificationAt() {
		return modification_at;
	}
	
	public void setModificationAt(Date modification_at) {
		this.modification_at = modification_at;
	}
	
	public void sendInvitation(Contact contact, Context context) {
		if(!new PGPSMSCipher(MainActivity.currentUserData).sendPublicKeyRequestMsg(contact.getNumber())){
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			AlertDialog dialog = alertDialogBuilder.create();
			dialog.setMessage("Error sending invitation to " + contact.getNumber());
			dialog.show();
		}
	}
	
	//database methods for creating table
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(REQUESTS_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_REQUESTS);
    	onCreate(database);
  	}
	
	//toString on purpose of arrayAdapter in ListView
	public String toString() {
		return contact_id + ": " + status;
	}
}
