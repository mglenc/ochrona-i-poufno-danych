package pl.edu.pwr.szyfrowaniesms.bean;

import android.database.sqlite.SQLiteDatabase;
import java.io.Serializable;

public class Contact implements Serializable {
	private long id;
	private String fullname;
	private String number;
	private String pub_key;
	
	private static final long serialVersionUID = 1L;
	
	public static final String TABLE_CONTACTS = "contacts";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_FULLNAME = "fullname";
	public static final String COLUMN_NUMBER = "number";
	public static final String COLUMN_PUBKEY = "pub_key";
	
	private static final String CONTACTS_CREATE = "create table " + TABLE_CONTACTS + "("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_FULLNAME + " text not null, "
			+ COLUMN_NUMBER + " text not null, "
			+ COLUMN_PUBKEY + " text null);";
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getFullname() {
		return fullname;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getPubKey() {
		return pub_key;
	}
	
	public void setPubKey(String pub_key) {
		this.pub_key = pub_key;
	}
	
	//database methods for creating table
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(CONTACTS_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
    	onCreate(database);
  	}
	
	//toString on purpose of arrayAdapter in ListView
	public String toString() {
		return fullname;
	}
}
