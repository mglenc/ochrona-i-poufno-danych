package pl.edu.pwr.szyfrowaniesms.bean;

import java.util.Date;

import pl.edu.pwr.szyfrowaniesms.MainActivity;
import pl.edu.pwr.szyfrowaniesms.cipher.PGPSMSCipher;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class Message {
	private long id;
	private long contact_id;
	private String content;
	private Date datetime;
	private long to_user;
	
	public static final String TABLE_MESSAGES = "messages";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_CONTACT = "contact";
	public static final String COLUMN_CONTENT = "content";
	public static final String COLUMN_DATETIME = "datetime";
	public static final String COLUMN_TO_USER = "to_user";
	
	private static final String MESSAGES_CREATE = "create table " + TABLE_MESSAGES + "("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_CONTACT + " integer not null, "
			+ COLUMN_CONTENT + " text, "
			+ COLUMN_DATETIME + " datetime default CURRENT_TIMESTAMP, "
			+ COLUMN_TO_USER + " integer not null, "
			+ " FOREIGN KEY (" + COLUMN_CONTACT + ") REFERENCES " + Contact.TABLE_CONTACTS + " (" + Contact.COLUMN_ID + "));";
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getContactId() {
		return contact_id;
	}
	
	public void setContactId(long contact_id) {
		this.contact_id = contact_id;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public Date getDatetime() {
		return datetime;
	}
	
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	
	public long getToUser() {
		return to_user;
	}
	
	public void setToUser(long to_user) {
		this.to_user = to_user;
	}
	
	public void sendMessage(Contact contact, Context context) {
		PGPSMSCipher pgpsmscipher = new PGPSMSCipher(MainActivity.currentUserData);
		pgpsmscipher.sendEncryptedMsg(this);
	}
	
	//database methods for creating table
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(MESSAGES_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
    	onCreate(database);
  	}
	
	//toString on purpose of arrayAdapter in ListView
	public String toString() {
		return content;
	}
}
