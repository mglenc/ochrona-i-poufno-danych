package pl.edu.pwr.szyfrowaniesms.bean;

import java.io.Serializable;

import android.database.sqlite.SQLiteDatabase;

public class UserData implements Serializable {
	private Long id;
	private String number;
	private String password;
	private String plainPassword;
	private String publicKey;
	private String privateKey;
	
	private static final long serialVersionUID = 1L;
	
	public static final String TABLE_USERDATA = "user_data";
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NUMBER = "number";
	public static final String COLUMN_PASSWORD = "password";
	public static final String COLUMN_PUBLICKEY = "public_key";
	public static final String COLUMN_PRIVATEKEY = "private_key";
	
	private static final String USERDATA_CREATE = "create table " + TABLE_USERDATA + "("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_PASSWORD + " text not null, "
			+ COLUMN_NUMBER + " text not null, "
			+ COLUMN_PUBLICKEY + " text not null, "
			+ COLUMN_PRIVATEKEY + " text not null);";
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPlainPassword() {
		return plainPassword;
	}
	
	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}
	
	public String getPublicKey() {
		return publicKey;
	}
	
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	
	public String getPrivateKey() {
		return privateKey;
	}
	
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	
	//database methods for creating table
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(USERDATA_CREATE);
	}
	
	public static void onUpgrade(SQLiteDatabase database) {
		database.execSQL("DROP TABLE IF EXISTS " + USERDATA_CREATE);
    	onCreate(database);
  	}
}
