package pl.edu.pwr.szyfrowaniesms.cipher;

import java.security.KeyPair;

import org.spongycastle.openpgp.PGPKeyRingGenerator;
import org.spongycastle.util.encoders.Base64;

import pl.edu.pwr.szyfrowaniesms.SzyfrowanieSMS;
import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Message;
import pl.edu.pwr.szyfrowaniesms.bean.UserData;
import pl.edu.pwr.szyfrowaniesms.datasource.ContactsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.MessagesDataSource;
import android.telephony.SmsManager;
import android.util.Log;

public class PGPSMSCipher extends SMSCipher {
	public static final String TAG = "PGPSMSCipher";
	public static final String MSG_PUBLIC_KEY_REQUEST_PREFIX = "-----PUBLIC KEY REQUEST-----";
	public static final String MSG_PUBLIC_KEY_PREFIX = "-----BEGIN PGP PUBLIC KEY BLOCK-----";
	public static final String MSG_PUBLIC_KEY_SUFFIX = "-----END PGP PUBLIC KEY BLOCK-----";
	public static final String MSG_ENCRYPTED_PREFIX = "-----BEGIN PGP MESSAGE-----";
	public static final String MSG_ENCRYPTED_SUFFIX = "-----END PGP MESSAGE-----";
	public static final String IDENTITY_SUFFIX = "@szyfrowaniesms.pwr.edu.pl";

	public PGPSMSCipher(UserData userData) {
		super(userData);
	}
	
	@Override
	public boolean generateKeyPair(String password) {
		try {
			KeyPair rsaKeyPar = PGPHelper.generateEncryptionKeyPair(1024);
			KeyPair dsaKeyPair = PGPHelper.generateSignatureKeyPair(1024);
			PGPKeyRingGenerator pgpKeyRingGen = PGPHelper
					.createPGPKeyRingGenerator(rsaKeyPar, dsaKeyPair,
							getUserData().getNumber()+IDENTITY_SUFFIX,
							password.toCharArray());

			byte[] secretKey = PGPHelper.exportSecretKey(pgpKeyRingGen);
			byte[] publicKey = PGPHelper.exportPublicKey(pgpKeyRingGen);

			getUserData().setPrivateKey(new String(secretKey));
			getUserData().setPublicKey(new String(publicKey));
			
			//TODO: save keys to user_data table in sqlite
			return true;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	@Override
	public boolean sendPublicKeyRequestMsg(String phoneNumber) {
		try {
			SmsManager.getDefault().sendTextMessage(phoneNumber, null,
					MSG_PUBLIC_KEY_REQUEST_PREFIX, null, null);
			return true;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	@Override
	public boolean sendPublicKeyMsg(String phoneNumber) {
		try {
			SmsManager smsMan = SmsManager.getDefault();
			smsMan.sendMultipartTextMessage(phoneNumber, null,
					smsMan.divideMessage(getUserData().getPublicKey()), null, null);
			return true;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return false;
		}
	}

	@Override
	public boolean isPublicKeyRequestMsg(String smsMsgContent) {
		if(null == smsMsgContent || smsMsgContent.isEmpty()){
			return false;
		}
		return smsMsgContent.equals(MSG_PUBLIC_KEY_REQUEST_PREFIX);
	}

	@Override
	public boolean isPublicKeyMsg(String smsMsgContent) {
		if(null == smsMsgContent || smsMsgContent.isEmpty()){
			return false;
		}
		return smsMsgContent.trim().startsWith(MSG_PUBLIC_KEY_PREFIX)
				&& smsMsgContent.trim().endsWith(MSG_PUBLIC_KEY_SUFFIX);
	}

	@Override
	public boolean isEncryptedMsg(String smsMsgContent) {
		if(null == smsMsgContent || smsMsgContent.isEmpty()){
			return false;
		}
		return smsMsgContent.startsWith(MSG_ENCRYPTED_PREFIX)
				&& smsMsgContent.endsWith(MSG_ENCRYPTED_SUFFIX);
	}

	@Override
	public boolean savePublicKey(String fullName, String phoneNumber, String publicKey) {
		ContactsDataSource cds = null;
		try{
			cds = new ContactsDataSource(SzyfrowanieSMS.getAppContext());
			cds.open();
			Contact contact = cds.getContactByNumber(phoneNumber);
			if (null == contact) {
				return null != cds.createContact(fullName, phoneNumber, publicKey);
			} else {
				contact.setPubKey(publicKey);
				return cds.updateContact(contact);
			}
		} finally{
			if(null != cds){
				cds.close();
			}
		}
	}

	@Override
	public boolean isNumberTrusted(String phoneNumber) {
		ContactsDataSource cds = null;
		try{
			cds = new ContactsDataSource(SzyfrowanieSMS.getAppContext());
			cds.open();
			Contact contact = cds.getContactByNumber(phoneNumber);
			return null != contact && null != contact.getPubKey() && !contact.getPubKey().isEmpty();
		} finally{
			if(null != cds){
				cds.close();
			}
		}
	}

	@Override
	public boolean sendEncryptedMsg(Message message) {
		long contact_id = message.getContactId();
		
		if (null == message.getContent() || message.getContent().isEmpty()) {
			return false;
		}
		byte[] secretKey = getUserData().getPrivateKey().getBytes();

		Contact contact = null;
		ContactsDataSource cds = null;
		
		MessagesDataSource mds = null;
		
		try {
			cds = new ContactsDataSource(SzyfrowanieSMS.getAppContext());
			cds.open();
			contact = cds.getContactById(contact_id);
			if (null == contact) {
				return false;
			}
			byte[] publicKey = contact.getPubKey().getBytes();
			//For sending - sign and encrypt
			byte[] signed = PGPHelper.signData(message.getContent().getBytes(), secretKey,
					getUserData().getPlainPassword().toCharArray(), false);
			byte[] encrypted = PGPHelper.encryptData(signed, publicKey, false,false);
			String msgToSend = MSG_ENCRYPTED_PREFIX + "\n"
					+ new String(Base64.encode(encrypted)) + "\n"
					+ MSG_ENCRYPTED_SUFFIX;
			//For local storage - no sign
			byte[] encryptedForStorage = PGPHelper.encryptData(message.getContent().getBytes(), getUserData().getPublicKey().getBytes(), false, false);
			String msgToSave = MSG_ENCRYPTED_PREFIX + "\n"
					+ new String(Base64.encode(encryptedForStorage)) + "\n"
					+ MSG_ENCRYPTED_SUFFIX; 
			
			//adding encrypted message to database
			mds = new MessagesDataSource(SzyfrowanieSMS.getAppContext());
			mds.open();
			message.setContent(msgToSave);
			mds.updateMessage(message);
			
			SmsManager smsMan = SmsManager.getDefault();
			smsMan.sendMultipartTextMessage(contact.getNumber(), null,
					smsMan.divideMessage(msgToSend), null, null);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, e.getMessage());
			return false;
		} finally {
			if (null != cds) {
				cds.close();
			}
			if (null != mds) {
				mds.close();
			}
		}
	}

	@Override
	public String decryptMsg(String phoneNumber, String smsMsgContent) {
		if (!isEncryptedMsg(smsMsgContent)) {
			return null;
		}
		byte[] secretKey = getUserData().getPrivateKey().getBytes();
				
		smsMsgContent = smsMsgContent.replaceAll(MSG_ENCRYPTED_PREFIX + "\\n", "");
		smsMsgContent = smsMsgContent.replaceAll("\\n" + MSG_ENCRYPTED_SUFFIX, "");

		Contact contact = null;
		ContactsDataSource cds = null;
		try {
			cds = new ContactsDataSource(SzyfrowanieSMS.getAppContext());
			cds.open();
			contact = cds.getContactByNumber(phoneNumber);
			if (null == contact) {
				return null;
			}
			byte[] publicKey = contact.getPubKey().getBytes();
			byte[] decrypted = PGPHelper.decryptData(Base64.decode(smsMsgContent.getBytes()), secretKey, getUserData().getPlainPassword().toCharArray());
			StringBuilder plainBody = new StringBuilder();
			if(PGPHelper.verifySign(decrypted, publicKey, plainBody)){
				return plainBody.toString();
			}
			return null;
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return null;
		} finally {
			if (null != cds) {
				cds.close();
			}
		}
	}
	
	@Override
	public String decryptUserMsg(String smsMsgContent) {
		if (!isEncryptedMsg(smsMsgContent)) {
			return null;
		}
		byte[] secretKey = getUserData().getPrivateKey().getBytes();
		smsMsgContent = smsMsgContent.replaceAll(MSG_ENCRYPTED_PREFIX + "\\n", "");
		smsMsgContent = smsMsgContent.replaceAll("\\n" + MSG_ENCRYPTED_SUFFIX, "");

		try {
			byte[] decrypted = PGPHelper.decryptData(Base64.decode(smsMsgContent.getBytes()), secretKey, getUserData().getPlainPassword().toCharArray());
			return new String(decrypted);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			return null;
		}
	}
}
