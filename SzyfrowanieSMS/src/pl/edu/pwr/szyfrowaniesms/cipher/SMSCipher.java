package pl.edu.pwr.szyfrowaniesms.cipher;

import pl.edu.pwr.szyfrowaniesms.bean.Message;
import pl.edu.pwr.szyfrowaniesms.bean.UserData;

/**
 * 
 */
public abstract class SMSCipher {
	/**
	 * 
	 */
	private UserData userData;
	
	/**
	 * 
	 * @param userData
	 */
	public SMSCipher(UserData userData) {
		this.userData = userData;
	}
	
	/**
	 * 
	 * @return
	 */
	public UserData getUserData() {
		return userData;
	}
	
	/**
	 * 
	 * @param password
	 * @return
	 */
	public abstract boolean generateKeyPair(String password);
	/**
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public abstract boolean sendPublicKeyRequestMsg(String phoneNumber);
	/**
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public abstract boolean sendPublicKeyMsg(String phoneNumber);
	/**
	 * 
	 * @param content
	 * @return
	 */
	public abstract boolean isPublicKeyRequestMsg(String smsMsgContent);
	/**
	 * 
	 * @param content
	 * @return
	 */
	public abstract boolean isPublicKeyMsg(String smsMsgContent);
	/**
	 * 
	 * @param content
	 * @return
	 */
	public abstract boolean isEncryptedMsg(String smsMsgContent);
	/**
	 * 
	 * @param fullName
	 * @param phoneNumber
	 * @param publicKey
	 * @return
	 */
	public abstract boolean savePublicKey(String fullName, String phoneNumber, String publicKey);
	/**
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public abstract boolean isNumberTrusted(String smsMsgContent);
	/**
	 * 
	 * @param phoneNumber
	 * @param content
	 * @return
	 */
	public abstract boolean sendEncryptedMsg(Message message);
	/**
	 * 
	 * @param sms
	 * @return
	 */
	public abstract String decryptMsg(String phoneNumber, String smsMsgContent);
	/**
	 * 
	 * @param smsMsgContent
	 * @return
	 */
	public abstract String decryptUserMsg(String smsMsgContent);
}
