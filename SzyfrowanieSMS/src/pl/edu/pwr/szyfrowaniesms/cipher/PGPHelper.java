package pl.edu.pwr.szyfrowaniesms.cipher;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;
import java.util.Iterator;

import org.spongycastle.bcpg.ArmoredOutputStream;
import org.spongycastle.bcpg.BCPGOutputStream;
import org.spongycastle.bcpg.HashAlgorithmTags;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.openpgp.PGPCompressedData;
import org.spongycastle.openpgp.PGPCompressedDataGenerator;
import org.spongycastle.openpgp.PGPEncryptedData;
import org.spongycastle.openpgp.PGPEncryptedDataGenerator;
import org.spongycastle.openpgp.PGPEncryptedDataList;
import org.spongycastle.openpgp.PGPException;
import org.spongycastle.openpgp.PGPKeyPair;
import org.spongycastle.openpgp.PGPKeyRingGenerator;
import org.spongycastle.openpgp.PGPLiteralData;
import org.spongycastle.openpgp.PGPLiteralDataGenerator;
import org.spongycastle.openpgp.PGPObjectFactory;
import org.spongycastle.openpgp.PGPOnePassSignature;
import org.spongycastle.openpgp.PGPOnePassSignatureList;
import org.spongycastle.openpgp.PGPPrivateKey;
import org.spongycastle.openpgp.PGPPublicKey;
import org.spongycastle.openpgp.PGPPublicKeyEncryptedData;
import org.spongycastle.openpgp.PGPPublicKeyRing;
import org.spongycastle.openpgp.PGPPublicKeyRingCollection;
import org.spongycastle.openpgp.PGPSecretKey;
import org.spongycastle.openpgp.PGPSecretKeyRing;
import org.spongycastle.openpgp.PGPSecretKeyRingCollection;
import org.spongycastle.openpgp.PGPSignature;
import org.spongycastle.openpgp.PGPSignatureGenerator;
import org.spongycastle.openpgp.PGPSignatureList;
import org.spongycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.spongycastle.openpgp.PGPUtil;
import org.spongycastle.openpgp.operator.PGPDigestCalculator;
import org.spongycastle.openpgp.operator.jcajce.JcaPGPContentSignerBuilder;
import org.spongycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;
import org.spongycastle.openpgp.operator.jcajce.JcaPGPDigestCalculatorProviderBuilder;
import org.spongycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.spongycastle.openpgp.operator.jcajce.JcePBESecretKeyEncryptorBuilder;
import org.spongycastle.openpgp.operator.jcajce.JcePGPDataEncryptorBuilder;
import org.spongycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;
import org.spongycastle.openpgp.operator.jcajce.JcePublicKeyKeyEncryptionMethodGenerator;
import org.spongycastle.util.io.Streams;

public class PGPHelper {
	private static final int BUFFER_SIZE = 1 << 16;
	
	static {
        Security.addProvider(new BouncyCastleProvider());
    }
	
	private PGPHelper() {
	}
	
   public static final KeyPair generateEncryptionKeyPair(int keySize) throws NoSuchAlgorithmException, NoSuchProviderException{
       KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA", "SC");
       keyPairGenerator.initialize(keySize);
       KeyPair keyPair = keyPairGenerator.generateKeyPair();
       return keyPair;
   }
   
   public static final KeyPair generateSignatureKeyPair(int keySize) throws NoSuchAlgorithmException, NoSuchProviderException{
       KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA", "SC");
       keyPairGenerator.initialize(keySize);
       KeyPair keyPair = keyPairGenerator.generateKeyPair();
       return keyPair;
   }
   
   	public static final PGPKeyRingGenerator createPGPKeyRingGenerator(KeyPair rsaKeyPair, KeyPair dsaKeyPair, String identity, char[] passphrase) throws Exception {
       PGPKeyPair rsaPgpKeyPair = new PGPKeyPair(PGPPublicKey.RSA_ENCRYPT, rsaKeyPair, new Date(), "SC");
       PGPKeyPair dsaPgpKeyPair = new PGPKeyPair(PGPPublicKey.DSA, dsaKeyPair, new Date(), "SC");
       PGPDigestCalculator sha1Calc = new JcaPGPDigestCalculatorProviderBuilder().build().get(HashAlgorithmTags.SHA1);
       PGPKeyRingGenerator keyRingGen = new PGPKeyRingGenerator(
    		   PGPSignature.POSITIVE_CERTIFICATION,
    		   dsaPgpKeyPair,
    		   identity,
    		   sha1Calc,
    		   null,
    		   null,
    		   new JcaPGPContentSignerBuilder(dsaPgpKeyPair.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA256),
    		   new JcePBESecretKeyEncryptorBuilder(PGPEncryptedData.CAST5, sha1Calc).setProvider("SC").build(passphrase)
    		   );
       keyRingGen.addSubKey(rsaPgpKeyPair);
       return keyRingGen;
   	}
   
	public static final byte[] exportSecretKey(PGPKeyRingGenerator pgpKeyRingGen) throws Exception {
		PGPSecretKeyRing pgpSecKeyRing = pgpKeyRingGen.generateSecretKeyRing();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ArmoredOutputStream aos = new ArmoredOutputStream(bos);
		pgpSecKeyRing.encode(aos);
		aos.close();
		bos.close();
		return bos.toByteArray();
	}

	public static final byte[] exportPublicKey(PGPKeyRingGenerator pgpKeyRingGen) throws IOException {
		PGPPublicKeyRing pgpPubKeyRing = pgpKeyRingGen.generatePublicKeyRing();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ArmoredOutputStream aos = new ArmoredOutputStream(bos);
		pgpPubKeyRing.encode(aos);
		aos.close();
		bos.close();
		return bos.toByteArray();
	}
	
    public static final byte[] signData(byte[] dataToSign, byte[] pgpKeyRing, char[] passphrase, boolean ascii) throws Exception {
        //KeyRing i klucz
    	InputStream keyInputStream = new ByteArrayInputStream(pgpKeyRing);
        PGPSecretKey pgpSecretKey = readSecretKey(keyInputStream);
        
        //Klucz prywatny
        PGPPrivateKey pgpPrivateKey = pgpSecretKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder().setProvider("SC").build(passphrase));
       
        //Generator podpisu
        PGPSignatureGenerator signatureGenerator = new PGPSignatureGenerator(new JcaPGPContentSignerBuilder(pgpSecretKey.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA256).setProvider("SC"));
        signatureGenerator.init(PGPSignature.BINARY_DOCUMENT, pgpPrivateKey);
 
        //Podpakiety z ID kolejnych podpisow klucza publicznego
        Iterator<String> it = pgpSecretKey.getPublicKey().getUserIDs();
        if (it.hasNext()) {
            PGPSignatureSubpacketGenerator  spGen = new PGPSignatureSubpacketGenerator();
            spGen.setSignerUserID(false, it.next());
            signatureGenerator.setHashedSubpackets(spGen.generate());
        }
         
        //Streamy do zapisu
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        OutputStream outputStream = null;
        if (ascii) {
            outputStream = new ArmoredOutputStream(bos);
        } else {
            outputStream = new BufferedOutputStream(bos);
        }
         
        //Generator danych skompresowanych
        PGPCompressedDataGenerator  compressDataGenerator = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);
        BCPGOutputStream bcOutputStream = new BCPGOutputStream(compressDataGenerator.open(outputStream));
        
        //Zapis pakietu pozwalajacego drugiej osobie na weryfikacje podpisu
        signatureGenerator.generateOnePassVersion(false).encode(bcOutputStream);
 
        //Generator danych
        PGPLiteralDataGenerator literalDataGenerator = new PGPLiteralDataGenerator();
        OutputStream literalDataGenOutputStream = literalDataGenerator.open(bcOutputStream, PGPLiteralData.BINARY, "", new Date(), new byte[BUFFER_SIZE]);
        InputStream bis = new ByteArrayInputStream(dataToSign);
         
        //Zapis danych
        int b;
        while ((b = bis.read()) != -1) {
            literalDataGenOutputStream.write(b);
            signatureGenerator.update((byte)b);
        }
        literalDataGenerator.close();
        bis.close();
 
        //Zapis podpisu
        signatureGenerator.generate().encode(bcOutputStream);
        
        compressDataGenerator.close();
        outputStream.close();
        
        return bos.toByteArray();
    }
    
    public static byte[] encryptData(byte[] dataToEncrypt, byte[] pgpKeyRing, boolean ascii, boolean integrityCheck) throws IOException, PGPException {
    	InputStream keyInputStream = new ByteArrayInputStream(pgpKeyRing);
        PGPPublicKey publicKey = readPublicKey(keyInputStream);
 
        //Streamy do zapisu
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        OutputStream outputStream = null;
        if (ascii) {
            outputStream = new ArmoredOutputStream(bos);
        }else {
            outputStream = new BufferedOutputStream(bos);
        }
 
        JcePGPDataEncryptorBuilder PgpDataEncryptorBuilder = new JcePGPDataEncryptorBuilder(PGPEncryptedData.CAST5)
        .setWithIntegrityPacket(integrityCheck)
        .setSecureRandom(new SecureRandom())
        .setProvider("SC");
 
        PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(PgpDataEncryptorBuilder);
        encryptedDataGenerator.addMethod(new JcePublicKeyKeyEncryptionMethodGenerator(publicKey).setProvider("SC"));
        OutputStream dataGeneratorOut = encryptedDataGenerator.open(outputStream, new byte[BUFFER_SIZE]);
        
        PGPCompressedDataGenerator  compressDataGeneratorOut = new PGPCompressedDataGenerator(PGPCompressedData.ZIP);
        PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
        OutputStream pOut = lData.open(compressDataGeneratorOut.open(dataGeneratorOut), PGPLiteralData.BINARY, "", new Date(), new byte[BUFFER_SIZE]);
        pOut.write(dataToEncrypt);
        lData.close();
        compressDataGeneratorOut.close();
        dataGeneratorOut.close();
        outputStream.close();
        return bos.toByteArray();
    }
    
    public static final boolean verifySign(byte[] dataToVerify, byte[] publicKey, StringBuilder body) throws Exception {
        InputStream in = PGPUtil.getDecoderStream(new ByteArrayInputStream(dataToVerify));
         
        PGPObjectFactory pgpObjFactory = new PGPObjectFactory(in);
        PGPCompressedData compressedData = (PGPCompressedData)pgpObjFactory.nextObject();
         
        //One-Pass-Signature
        pgpObjFactory = new PGPObjectFactory(compressedData.getDataStream());
        PGPOnePassSignatureList onePassSignatureList = (PGPOnePassSignatureList)pgpObjFactory.nextObject();
        PGPOnePassSignature onePassSignature = onePassSignatureList.get(0);
         
        //Dane
        PGPLiteralData pgpLiteralData = (PGPLiteralData)pgpObjFactory.nextObject();
        InputStream literalDataStream = pgpLiteralData.getInputStream();
         
        InputStream keyIn = new ByteArrayInputStream(publicKey);
        PGPPublicKeyRingCollection pgpRing = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(keyIn));
        PGPPublicKey key = pgpRing.getPublicKey(onePassSignature.getKeyID());
         
        ByteArrayOutputStream literalDataOutputStream = new ByteArrayOutputStream();
        onePassSignature.init(new JcaPGPContentVerifierBuilderProvider().setProvider("SC"), key);
 
        int b;
        while ((b = literalDataStream.read()) != -1) {
            onePassSignature.update((byte)b);
            literalDataOutputStream.write(b);
        }
        literalDataOutputStream.close();
        //Zapis odczytanej tresci
        if(null != body){
        	body.append(new String(literalDataOutputStream.toByteArray()));
        }
         
        //Podpis
        PGPSignatureList p3 = (PGPSignatureList)pgpObjFactory.nextObject();
        PGPSignature signature = p3.get(0);
         
        //Weryfikacja
        return onePassSignature.verify(signature);
    }
    
    public static byte[] decryptData(byte[] dataToDecrypt, byte[] pgpKeyRing, char[] passphrase) throws IOException, PGPException {
        InputStream keyIn = new ByteArrayInputStream(pgpKeyRing);
        InputStream in = PGPUtil.getDecoderStream(new ByteArrayInputStream(dataToDecrypt));
 
        PGPObjectFactory pgpObjFactory = new PGPObjectFactory(in);
        PGPEncryptedDataList pgpEncryptedDataList = null;
 
        Object o = pgpObjFactory.nextObject();
        if (o instanceof PGPEncryptedDataList) {
            pgpEncryptedDataList = (PGPEncryptedDataList)o;
        }
        else {
            pgpEncryptedDataList = (PGPEncryptedDataList)pgpObjFactory.nextObject();
        }
 
        PGPPrivateKey secretKey = null;
        PGPPublicKeyEncryptedData publicKeyEncryptedData = null;
        PGPSecretKeyRingCollection pgpSecretKeyRingCollection = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(keyIn));
 
        Iterator<PGPPublicKeyEncryptedData> it = pgpEncryptedDataList.getEncryptedDataObjects();
        while(it.hasNext() && secretKey == null) {
            publicKeyEncryptedData = it.next();
            PGPSecretKey pgpSecKey = pgpSecretKeyRingCollection.getSecretKey(publicKeyEncryptedData.getKeyID());
 
            if (pgpSecKey != null) {
                Provider provider = Security.getProvider("SC");
                secretKey = pgpSecKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder(new JcaPGPDigestCalculatorProviderBuilder().setProvider(provider).build()).setProvider(provider).build(passphrase));
            }
        }
        if (secretKey == null) {
            throw new IllegalArgumentException("secret key for message not found.");
        }
        if(publicKeyEncryptedData == null) {
            throw new NullPointerException("cannot continue with null public key encryption data.");
        }
 
        InputStream clear = publicKeyEncryptedData.getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder().setProvider("SC").build(secretKey));
        PGPObjectFactory plainFact = new PGPObjectFactory(clear);
        PGPCompressedData compressedData = (PGPCompressedData)plainFact.nextObject();
        InputStream compressedStream = new BufferedInputStream(compressedData.getDataStream());
        PGPObjectFactory pgpFact = new PGPObjectFactory(compressedStream);
        Object message = pgpFact.nextObject();
 
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (message instanceof PGPLiteralData) {
            PGPLiteralData literalData = (PGPLiteralData)message;
            InputStream literalDataInputStream = literalData.getInputStream();
            OutputStream out = new BufferedOutputStream(bos);
            Streams.pipeAll(literalDataInputStream, out);
            out.close();
        } else if (message instanceof PGPOnePassSignatureList) {
            throw new PGPException("encrypted message contains a signed message - not literal data.");
        }
        else {
            throw new PGPException("message is not a simple encrypted file - type unknown.");
        }
 
        if (publicKeyEncryptedData.isIntegrityProtected()) {
            if (!publicKeyEncryptedData.verify()) {
                throw new PGPException("message failed integrity check");
            }
        }
 
        keyIn.close();
        in.close();
        
        return bos.toByteArray();
    }
	
    private static PGPSecretKey readSecretKey(InputStream input) throws IOException, PGPException {
        PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(input));
        Iterator<PGPSecretKeyRing> it = pgpSec.getKeyRings();
        PGPSecretKey secKey = null;
        while (it.hasNext() && secKey == null) {
            PGPSecretKeyRing keyRing = it.next();
            Iterator<PGPSecretKey> keyIt = keyRing.getSecretKeys();
            while (keyIt.hasNext()) {
                PGPSecretKey key = keyIt.next();
                if (key.isSigningKey()) {
                    secKey = key;
                    break;
                }
            }
        }
        if(secKey != null) {
            return secKey;
        } else {
            throw new IllegalArgumentException("Can't find signing key in key ring!");
        }
    }
    
    private static PGPPublicKey readPublicKey(InputStream input) throws IOException, PGPException
    {
        PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(input));
        PGPPublicKey pubKey = null;
        Iterator<PGPPublicKeyRing> keyRingIter = pgpPub.getKeyRings();
        while (keyRingIter.hasNext() && pubKey == null) {
            PGPPublicKeyRing keyRing = keyRingIter.next();
            Iterator<PGPPublicKey> keyIter = keyRing.getPublicKeys();
            while (keyIter.hasNext()) {
                PGPPublicKey key = keyIter.next();
                if (key.isEncryptionKey()) {
                    pubKey = key;
                    break;
                }
            }
        }
        if(pubKey != null) {
            return pubKey;
        } else {
            throw new IllegalArgumentException("Can't find encryption key in key ring.");
        }
    }
}
