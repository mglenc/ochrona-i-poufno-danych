package pl.edu.pwr.szyfrowaniesms;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class RequestWaitActivity extends Activity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_wait);
	}
	
	 @Override
	 protected void onPause() {
		 super.onPause();
	 }
	 
	 @Override
	 protected void onResume() {
    	super.onResume();
    	
    	final Contact contact = (Contact) getIntent().getSerializableExtra("contact");
    	
    	final TextView requestWaitNumberTextView = (TextView) findViewById(R.id.request_wait_number);
    	requestWaitNumberTextView.setText(contact.getNumber());
	 }
}
