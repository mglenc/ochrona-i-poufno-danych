package pl.edu.pwr.szyfrowaniesms;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Message;
import pl.edu.pwr.szyfrowaniesms.bean.Request;
import pl.edu.pwr.szyfrowaniesms.cipher.PGPSMSCipher;
import pl.edu.pwr.szyfrowaniesms.datasource.ContactsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.MessagesDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.RequestsDataSource;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class SMSListener extends BroadcastReceiver {
    private SharedPreferences preferences;
    private ContactsDataSource contacts_datasource;
    private RequestsDataSource requests_datasource;
    private MessagesDataSource messages_datasource;
    
    @Override
    public void onReceive(Context context, Intent intent) {
    	contacts_datasource = new ContactsDataSource(context);
    	requests_datasource = new RequestsDataSource(context);
    	messages_datasource = new MessagesDataSource(context);
    	
    	contacts_datasource.open();
    	requests_datasource.open();
    	messages_datasource.open();
    	
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            if (bundle != null) {
            		Map<String, String> receivedMap= new HashMap<String, String>();
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        String messageFrom = msgs[i].getOriginatingAddress();
                        if(!receivedMap.containsKey(messageFrom)){
                        	receivedMap.put(messageFrom, msgs[i].getMessageBody());
                        } else {
                        	String previousBody = receivedMap.get(messageFrom);
                        	receivedMap.put(messageFrom, previousBody + msgs[i].getMessageBody()); 
                        }
                    }
                    
                    PGPSMSCipher pgpsmscipher = new PGPSMSCipher(MainActivity.currentUserData);
                    
                    //Key - number, Value - msg body
                    for(Entry<String, String> entry : receivedMap.entrySet()){
                    	String messageFrom = entry.getKey();
                    	String allMessageBody = entry.getValue();
                    	Log.i("Message received", allMessageBody);
                    	if(pgpsmscipher.isPublicKeyRequestMsg(allMessageBody)) {
                    		Log.i("Request from number", messageFrom);
                    		
                    		//Add contact for request
                    		Contact contact = contacts_datasource.createContact(messageFrom, messageFrom, null);
                    		String content = "This contact requests your public key.";
                    	                        	
                    		//Add request to database
                    		Request request = requests_datasource.createRequest(contact, content, 3);
                    	
                    		//Date now = new Date();
                    	
                    		//Add message to user with public key request
                    		//Message message = messages_datasource.createMessage(contact, content, now, 1);
                    		
                    		//TO DO: Refreshing contact list when its active does not work, sometimes
                    	
                    		MainActivity.contactlistAdapter.add(contact);
                    		MainActivity.contactlistAdapter.notifyDataSetChanged();
                    	} else if(pgpsmscipher.isPublicKeyMsg(allMessageBody)) {
                    		Log.i("Public key received from", messageFrom);
                    		
                    		//add public key to contact
                    		Contact contact = contacts_datasource.getContactByNumber(messageFrom);
                    		Request request = requests_datasource.getRequestByContact(contact);
                    		
                    		contact.setPubKey(allMessageBody);
                    		contacts_datasource.updateContact(contact);

                    		// sending my (user) public key to contact
                    		if (request.getStatus() == 0) {
                    			pgpsmscipher.sendPublicKeyMsg(contact.getNumber());
                    			request.setStatus(1);
                    			requests_datasource.updateRequest(request);
                    		}
                    	} else if(pgpsmscipher.isEncryptedMsg(allMessageBody)) {
                    		Log.i("Encrypted message received from", messageFrom);
                    		
                    		//get number contact
                    		Contact contact = contacts_datasource.getContactByNumber(messageFrom);
                    	
                    		Date now = new Date();
                    		
                    		//add encrypted message to database
                    		Message message = messages_datasource.createMessage(contact, allMessageBody, now, 1);
                    		
                    		if(ContactMessagesActivity.adapter != null) {
                    			if(ContactMessagesActivity.currentContact.getId() == contact.getId()) {
                    				ContactMessagesActivity.adapter.add(message);
                    				ContactMessagesActivity.adapter.notifyDataSetChanged();
                    			}
                    		}
                    	}
                    }
            	}
        	}	
        	contacts_datasource.close();
        	requests_datasource.close();
        	messages_datasource.close();
    }
}
