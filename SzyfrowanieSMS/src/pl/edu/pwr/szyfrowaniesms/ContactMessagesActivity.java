package pl.edu.pwr.szyfrowaniesms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.edu.pwr.szyfrowaniesms.bean.Contact;
import pl.edu.pwr.szyfrowaniesms.bean.Message;
import pl.edu.pwr.szyfrowaniesms.cipher.PGPSMSCipher;
import pl.edu.pwr.szyfrowaniesms.datasource.ContactsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.MessagesDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.RequestsDataSource;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ContactMessagesActivity extends Activity {
	private ContactsDataSource contacts_datasource;
	private RequestsDataSource requests_datasource;
	private MessagesDataSource messages_datasource;
	
	//Bubbles
	public static DiscussArrayAdapter adapter;
	private ListView lv;
	String testString;
	
	public static Contact currentContact;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactmessages_main);
		
		//loading contacts datasource
        contacts_datasource = new ContactsDataSource(this);
        
        //loading requests datasource
        requests_datasource = new RequestsDataSource(this);
        
        //loading messages datasource
        messages_datasource = new MessagesDataSource(this);
	}
	
	 @Override
	 protected void onPause() {
		 contacts_datasource.close();
		 requests_datasource.close();
		 messages_datasource.close();
		 super.onPause();
	 }
	 
	 @Override
	 protected void onResume() {
    	super.onResume();
        
    	contacts_datasource.open();
    	requests_datasource.open();
    	messages_datasource.open();
    	
    	final Contact contact = (Contact) getIntent().getSerializableExtra("contact");
    	currentContact = contact;
    	
    	//Messages list
    	List<Message> messages = messages_datasource.getMessagesForContact(contact);
    	lv = (ListView) findViewById(R.id.messages_list);
    	
    	//adapter = new DiscussArrayAdapter(getApplicationContext(), R.layout.listitem_discuss);
    	adapter = new DiscussArrayAdapter(getApplicationContext(), R.layout.listitem_discuss, messages, contact);
    	
    	lv.setAdapter(adapter);
            	
    	final Button newMessageSendButton = (Button) findViewById(R.id.newMessageSendButton);
        final Context context = this;
    	
    	newMessageSendButton.setOnClickListener(new View.OnClickListener() {
  			@Override
  			public void onClick(View v) {
  				EditText messageField = (EditText)findViewById(R.id.newMessageContent);
  				String content = messageField.getText().toString();
  				if(null == content || content.isEmpty()){
  					return;
  				}
  				
  				//creating message object, unencrypted - updated in PGPSMSCipher
  				Message message = messages_datasource.createMessage(contact, content, new Date(), 0);
  				message.sendMessage(contact, context);
  				//clearing message field
  				messageField.setText("");
  				adapter.add(message);
  				adapter.notifyDataSetChanged();
  			}
  		});
	 }
	 
	public class DiscussArrayAdapter extends ArrayAdapter<Message> {
		private TextView messageContent;
		private LinearLayout wrapper;
		private List<Message> messages = new ArrayList<Message>();
		private PGPSMSCipher pgpsmscipher;
		private Contact contact;

		@Override
		public void add(Message object) {
			messages.add(object);
			super.add(object);
		}

		public DiscussArrayAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
		}

		public DiscussArrayAdapter(Context context, int textViewResourceId,
				List<Message> msgs, Contact c) {
			super(context, textViewResourceId);
			messages = msgs;
			contact = c;

			pgpsmscipher = new PGPSMSCipher(MainActivity.currentUserData);
		}

		public int getCount() {
			return this.messages.size();
		}

		public Message getItem(int index) {
			return this.messages.get(index);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			if (row == null) {
				LayoutInflater inflater = (LayoutInflater) this.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater
						.inflate(R.layout.listitem_discuss, parent, false);
			}

			wrapper = (LinearLayout) row.findViewById(R.id.wrapper);

			Message message = getItem(position);

			messageContent = (TextView) row.findViewById(R.id.message_content);

			if (message.getToUser() != 0) {
				// message to user
				// using contact key to decrypt
				String decrypted = pgpsmscipher.decryptMsg(contact.getNumber(),
						message.getContent());

				messageContent.setText(decrypted);
				messageContent.setBackgroundResource(R.drawable.bubble_yellow);
				wrapper.setGravity(Gravity.LEFT);
			} else {
				// message from user
				// using user key to decrypt
				String decrypted = pgpsmscipher.decryptUserMsg(message
						.getContent());

				messageContent.setText(decrypted);
				messageContent.setBackgroundResource(R.drawable.bubble_green);
				wrapper.setGravity(Gravity.RIGHT);
			}
			return row;
		}

		public Bitmap decodeToBitmap(byte[] decodedByte) {
			return BitmapFactory.decodeByteArray(decodedByte, 0,
					decodedByte.length);
		}
	}
}
