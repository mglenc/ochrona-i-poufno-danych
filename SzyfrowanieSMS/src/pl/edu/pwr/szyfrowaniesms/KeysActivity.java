package pl.edu.pwr.szyfrowaniesms;

import pl.edu.pwr.szyfrowaniesms.datasource.ContactsDataSource;
import pl.edu.pwr.szyfrowaniesms.datasource.UserDataDataSource;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

public class KeysActivity extends Activity {
	private ContactsDataSource contacts_datasource;
	private UserDataDataSource userdata_datasource;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//loading contacts datasource
        contacts_datasource = new ContactsDataSource(this);

        //loading user data datasource
        userdata_datasource = new UserDataDataSource(this);
        
        setContentView(R.layout.keys_main);
  	  
        //loading userdata keys into textfields
        final TextView userPrivateKeyTextView = (TextView) findViewById(R.id.user_private_key);
        userPrivateKeyTextView.setText(MainActivity.currentUserData.getPrivateKey());
       
        final TextView userPublicKeyTextView = (TextView) findViewById(R.id.user_public_key);
        userPublicKeyTextView.setText(MainActivity.currentUserData.getPublicKey());
        
		final Context context = this;
	}
	
	 @Override
	 protected void onPause() {
		 contacts_datasource.close();
		 userdata_datasource.close();
		 super.onPause();
	 }
	 
	 @Override
	 protected void onResume() {
    	super.onResume();
        
    	contacts_datasource.open();
    	userdata_datasource.open();
	 }
}

